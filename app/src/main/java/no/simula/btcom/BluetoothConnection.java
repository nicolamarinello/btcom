package no.simula.btcom;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class BluetoothConnection {

    private BluetoothAdapter mBluetoothAdapter;
    private final BluetoothManager bluetoothManager;
    private BluetoothLeScanner bluetoothLeScanner;
    private boolean mScanning;
    private Handler mHandler;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothDevice mBluetoothDevice;

    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_DATA_AVAILABLE = BluetoothConnection.class.getName() + ".ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = BluetoothConnection.class.getName() + ".EXTRA_DATA";

    //TODO: the string must include the package name

    private static final ParcelUuid UUID_NECKX              = ParcelUuid.fromString("aaf9222c-e3b5-4335-9857-d91fc6bcbaa9");
    private static final ParcelUuid UUID_NECKX_RAW          = ParcelUuid.fromString("aaf92064-e3b5-4335-9857-d91fc6bcbaa9");
    private static final ParcelUuid UUID_NECKX_RAW_VALUES   = ParcelUuid.fromString("aaf90273-e3b5-4335-9857-d91fc6bcbaa9");
    private static final ParcelUuid UUID_NECKX_STREAM       = ParcelUuid.fromString("AAF91D91-E3B5-4335-9857-D91FC6BCBAA9");
    private static final ParcelUuid UUID_NECKX_SETTINGS     = ParcelUuid.fromString("AAF9C4D8-E3B5-4335-9857-D91FC6BCBAA9");
    private static final ParcelUuid UUID_BATTERY_LEVEL      = ParcelUuid.fromString("00002a19-0000-1000-8000-00805f9b34fb");
    private static final ParcelUuid UUID_BATTERY            = ParcelUuid.fromString("0000180f-0000-1000-8000-00805f9b34fb");


    private boolean btsupported;
    private boolean blesupported;

    private Context context;

    private static final String TAG = "BluetoothConnection";

    //constructor
    public BluetoothConnection(Context cxt) {
        context=cxt;
        // Initializes Bluetooth adapter.
        bluetoothManager = (BluetoothManager)context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter != null) {btsupported=true;}  //check if BT is supported
        blesupported=context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE); //check if BLE is supported
    }

    //check if bt is supported
    public boolean isBtsupported(){return btsupported;}

    //check if BLE is supported
    public boolean isBlesupported(){return blesupported;}

    //check if Bluetooth is already on
    public boolean isBtEnabled(){return mBluetoothAdapter.isEnabled();}

    //start discovery process fro expain neck devices
    public void scanLeDevice(final boolean enable) {

        bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();

        mHandler = new Handler();

        //set filter for the specific device
        ScanFilter.Builder expainFilterBuilder = new ScanFilter.Builder();
        expainFilterBuilder.setServiceUuid(UUID_NECKX);
        ScanFilter expainFilter = expainFilterBuilder.build();

        final ArrayList<ScanFilter> filters = new ArrayList<>();
        filters.add(0,expainFilter);

        //no specific scan settings
        ScanSettings.Builder expainSettingsBuilder = new ScanSettings.Builder();
        final ScanSettings expainSettings=expainSettingsBuilder.build();


        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    bluetoothLeScanner.stopScan(callback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            bluetoothLeScanner.startScan(filters,expainSettings,callback);
        } else {
            mScanning = false;
            bluetoothLeScanner.stopScan(callback);
        }

    }

    //check if bluetooth is discovering
    //public boolean iDiscovering(){}

    //cancel bluetooth discovery
    //public boolean cDiscovery(){}

    public void batteryLevel(){

        BluetoothGattService batteryService = mBluetoothGatt.getService(UUID_BATTERY.getUuid());

        if(batteryService == null) {
            Log.d(TAG, "Battery service not found!");
            return;
        }

        BluetoothGattCharacteristic batteryLevel = batteryService.getCharacteristic(UUID_BATTERY_LEVEL.getUuid());
        if(batteryLevel == null) {
            Log.d(TAG, "Battery level not found!");
            return;
        }

        mBluetoothGatt.readCharacteristic(batteryLevel);

        Log.v(TAG, "gatt.readCharacteristic(batteryLevel)");
        //Float a = batteryLevel.getFloatValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        //Log.v(TAG, "Battery Level: " + batteryLevel.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0));


    }


    public void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic){

        final Intent intent = new Intent(action);

        if ((UUID_BATTERY_LEVEL.getUuid()).equals(characteristic.getUuid())){
            intent.putExtra(EXTRA_DATA, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 0));
        }

        context.sendBroadcast(intent);

    }


    public void btDestroy(){
        //cDiscovery(); //cancel discovery
        Log.d(TAG, "btDestroy()");
    }


    private final ScanCallback callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            mBluetoothDevice = result.getDevice();
            mBluetoothGatt = mBluetoothDevice.connectGatt(context, false, mGattCallback);

            //stop scanning after connection
            scanLeDevice(false);

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };


    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyUpdate(gatt, txPhy, rxPhy, status);

        }

        @Override
        public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyRead(gatt, txPhy, rxPhy, status);
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            Log.i("onConnectionStateChange", "Status: " + status);

            switch (newState){
                case BluetoothProfile.STATE_CONNECTED:
                    Log.d(TAG, "Device connected!");
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.d(TAG, "Device disconnected!");
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            //get a list with all the services
            //List<BluetoothGattService> services = gatt.getServices();

        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);

            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }

            //int charge = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 0);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }


        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
        }
    };

}
