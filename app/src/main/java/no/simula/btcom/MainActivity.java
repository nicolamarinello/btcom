package no.simula.btcom;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private Context cxt;

    private final static int REQUEST_ENABLE_BT = 1;
    private BluetoothConnection btconn;

    private TextView batteryLevel;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //progress bar for bluetooth pairing with the sensor
        progressBar =(ProgressBar)findViewById(R.id.progressBar);

        //setting up the button for discovery new devices
        final Button button = (Button)findViewById(R.id.button);
        final Button button2 = (Button)findViewById(R.id.button2);
        batteryLevel = (TextView)findViewById(R.id.textView2);


        //get application context
        cxt = getApplicationContext();

        //create object bluetooth connection
        btconn = new BluetoothConnection(cxt);


        //bluetooth checks
        if (!btconn.isBtsupported()) {
            /* Device does not support Bluetooth */
            CharSequence text = "Device does not support Bluetooth";
            Toast.makeText(cxt, text, Toast.LENGTH_SHORT).show();
            //TODO: close application after the message
        }
        else{

            // Use this check to determine whether BLE is supported on the device. Then
            // you can selectively disable BLE-related features.
            if (!btconn.isBlesupported()) {
                CharSequence text = "Device does not support Bluetooth Low Energy";
                Toast.makeText(cxt, text, Toast.LENGTH_SHORT).show();
            }

            //check if Bluetooth is already on
            if (!btconn.isBtEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

            //set listener on the button
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    btconn.scanLeDevice(true);
                    progressBar.setVisibility(View.VISIBLE);
                    Log.d(TAG, "scanLeDevice() ");
                }
            });

            button2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    btconn.batteryLevel();
                    Log.d(TAG, "batteryLevel()");
                }
            });

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BluetoothConnection.ACTION_DATA_AVAILABLE);
            registerReceiver(mGattUpdateReceiver,intentFilter);

        }//else

    }//onCreate




    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
        if (btconn.isBtsupported()) {
            btconn.btDestroy();
        }

    }//onDestroy


    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            switch (action) {
                case BluetoothConnection.ACTION_DATA_AVAILABLE:
                    batteryLevel.setText(intent.getIntExtra(BluetoothConnection.EXTRA_DATA, 0) + "%");
                    break;
            }

        }
    };


}



